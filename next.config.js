/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  distDir: 'build',
  output: 'standalone',
  images: {
    loader: 'akamai',
    path: '',
  },
}

module.exports = nextConfig

