// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import ytdl from 'ytdl-core';

export const config = {
  api: {
    responseLimit: false,
  },
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const id = req.query?.id;

  if (!id) {
    return res.status(400).json({
      success: false,
      message: 'Please provide a YouTube video ID',
    });
  }

  const result = ytdl.validateID(Array.isArray(id) ? id[0] : id);

  if (!result) {
    return res.status(404).json({
      success: false,
      message: `Sorry! Can not find YouTube video with this ID: ${id}`,
    });
  }

  try {
    const info = await ytdl.getInfo(Array.isArray(id) ? id[0] : id);
    const audioFormats = ytdl.filterFormats(info.formats, 'audioonly');

    if (audioFormats && audioFormats.length) {
      const response = await fetch(audioFormats[0].url)
      const arrayBuffer = await response.arrayBuffer()
      const buffer = Buffer.from(arrayBuffer);
      res.setHeader('Content-Type', 'audio/mpeg');
      res.send(buffer)
    } else {
      return res.status(502).json({
        success: false,
        message: 'Bad Gateway',
        data: null,
      });
    }
  } catch (error: any) {
    return res.status(500).json({
      success: false,
      message: error?.message || 'Something went wrong',
    });
  }
}
